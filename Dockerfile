FROM python

COPY . /app

WORKDIR /app

RUN make rebuild

EXPOSE 8000

CMD ["make", "start"]