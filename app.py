from flask import Flask, render_template, make_response
from config.settings import LOG, DB
from own import own_sqlite
from werkzeug.exceptions import HTTPException
import logging, time

logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s', filename='logs/%s.log'%(str(time.time())), level=LOG)

logging.info("Verification Database")
if (own_sqlite.bdd_exist(DB) != True):
    logging.info("Creating Database")
    own_sqlite.bdd_create(DB)

logging.info("Connecting Database")
bdd = own_sqlite.connect(DB)

app = Flask(__name__)

@app.errorhandler(HTTPException)
def page_not_found(error:HTTPException):
    response = make_response(render_template('http_error.jinja', description=error.description, httpCode=error.code, httpError=error.name))
    response.headers["X-Robots-Tag"] = "noindex"
    return response, error.code

@app.route('/')
def index():
    return render_template("base.jinja")
