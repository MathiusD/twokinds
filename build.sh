#!/bin/bash
# Setup This Repo
echo "Setup Repo";
FILE=./config/settings.py
if ! [ -f "$FILE" ]; then
    cp ./config/settings.sample.py $FILE;
fi
DIR=./logs
if ! [ -d "$DIR" ]; then
    mkdir $DIR;
fi
DIR=./data
if ! [ -d "$DIR" ]; then
    mkdir $DIR;
fi
git clone https://gitlab.com/MathiusD/owninstall;
cp owninstall/install.py own_install.py;
python3 build.py;
rm -rf owninstall;